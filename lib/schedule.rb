class Schedule
  attr_accessor :weeks
  attr_accessor :end_date

  def initialize(weeks = [])
    @weeks = weeks
  end

  def to_events
    update_week_ends
    weeks.flat_map(&:to_events)
  end

  def to_calendar
    cal = Icalendar::Calendar.new
    to_events.each { |event| cal.add_event(event) }
    cal
  end

  private

  def update_week_ends
    weeks.reverse.each_with_index do |week, index|
      week.week_ends_at = end_date - index.week
    end
  end
end
