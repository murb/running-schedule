class Week
  attr_reader :days
  attr_reader :week_start

  def initialize(monday = Day.new, tuesday = Day.new, wednesday = Day.new, thursday = Day.new, friday = Day.new, saturday = Day.new, sunday = Day.new)
    @days = [monday, tuesday, wednesday, thursday, friday, saturday, sunday]
  end

  def week_ends_at= date
    @week_start = date - 6.days
  end

  def to_events
    days.each_with_index do |day, index|
      day.date = week_start + index.day
    end
    days.flat_map(&:to_events)
  end
end
