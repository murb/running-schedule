class Day
  attr_accessor :tasks
  attr_accessor :date

  def initialize(tasks = [])
    @tasks = tasks
  end

  def to_events
    tasks.map do |task|
      event = task.to_event
      event.dtstart = Icalendar::Values::Date.new(date)
      event.dtend = Icalendar::Values::Date.new(date + 1.day)
      event
    end
  end
end
