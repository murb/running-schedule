class Task
  EASY_RUN = "slow run"
  INTERVALS = "intervals"
  FARTLEK = "fartlek"
  ENDURANCE = STEADY_RUN = "steady run"
  CROSS_TRAINING = "cross training"
  COMPETITIVE_RUN = "competitive run"

  TYPES = [EASY_RUN, ENDURANCE, CROSS_TRAINING, COMPETITIVE_RUN, FARTLEK, STEADY_RUN, INTERVALS]
  attr_accessor :type
  attr_accessor :duration # min
  attr_accessor :distance # km
  attr_accessor :title
  attr_accessor :comment
  attr_accessor :url

  def initialize(type:, duration: nil, distance: nil, comment: nil, title: nil, url: nil)
    @type = type
    @duration = duration
    @distance = distance
    @comment = comment
    @title = title
    @url = url
  end

  def summary
    if title
      title
    else
      strings = []
      strings << "#{distance}km" if distance
      strings << "#{duration}min" if duration
      strings << type
      strings.join(" ")
    end
  end

  def description
    type_comment = {
      EASY_RUN => "run relatively slowly, recovery",
      COMPETITIVE_RUN => "join a match, or run like you're in one",
      CROSS_TRAINING => "do something else, like swimming or cycling",
      ENDURANCE => "run at a steady pace, not too fast, not too slow",
      FARTLEK => "run irregularly fast, then slow, then fast, then slow",
      INTERVALS => "run using fixed times or distances fast, then slow, then fast, then slow"
    }
    [comment, type_comment[type]].compact.join(" ")
  end

  def to_event
    event = Icalendar::Event.new
    event.summary = summary
    event.description = description
    event.url = url
    event
  end
end
