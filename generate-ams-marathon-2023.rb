#!/usr/bin/env ruby
require "./generator"

end_date = Date.new(2023, 10, 15)

schedule = Schedule.new [
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 10)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 10)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 5)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 11)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 6)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 8)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 6)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 14)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 8)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 16)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 8)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::COMPETITIVE_RUN, distance: 12)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 10)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 19)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 10)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 20)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 11)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 16)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 11)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 26)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 6)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 13)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 26)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 6)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 13)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::COMPETITIVE_RUN, distance: 30, title: "30 van Amsterdam Noord", url: "https://www.30vanamsterdamnoord.nl/30")])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 6)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 14)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 29)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 8)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 14)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 22)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 8)]), Day.new, Day.new([Task.new(type: Task::STEADY_RUN, distance: 16)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 60)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 32)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 8)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 13)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 45)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 10)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 6)]), Day.new, Day.new([Task.new(type: Task::FARTLEK, distance: 10)]), Day.new, Day.new([Task.new(type: Task::CROSS_TRAINING, duration: 30)]), Day.new([Task.new(type: Task::ENDURANCE, distance: 13)])),
  Week.new(Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 5)]), Day.new, Day.new, Day.new, Day.new([Task.new(type: Task::EASY_RUN, distance: 3, comment: "3-5km")]), Day.new([Task.new(type: Task::COMPETITIVE_RUN, distance: 42.195, title: "Marathon!")]))
]
schedule.end_date = end_date

cal = schedule.to_calendar
filename = "schedule-#{end_date}.ics"
File.write filename, cal.to_ical
`./publish.sh` if File.exist? "publish.sh"
