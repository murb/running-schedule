# Runningschedule

Basic training schema (for e.g. running) generator ICS

## Requirements

Some recent version of ruby, with bundler

Run `bundle install` before running

## To generate

Copy an example and adjust it to your needs.

Then run it:

    ./generate-ams-marathon-2023.rb


## Tips

I copy the generated files automatically to my server, so I can share and access it. It is deployed using a `publish.sh` script.

    scp *.ics example.com:~/public_html/calendars
